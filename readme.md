uWeb
===

Demo: http://harryoh.bitbucket.org/uweb

Todo:

* demo site javascript error
* get size from api
* implement apis 

    yo angular-require:route myRoute
    yo angular-require:controller myController
    yo angular-require:directive myDirective
    yo angular-require:filter myFilter
    yo angular-require:service myService
    yo angular-require:view myView
    yo angular-require:decorator serviceName

License
======

https://github.com/faisalman/ua-parser-js: GPLv2 & MIT