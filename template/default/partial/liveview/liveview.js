angular.module('template.default')
  .controller('LiveviewCtrl', function ($scope, coreconfig) {
    /*
    var slot = 'onvif';
    coreconfig.setSlot(slot);
    $scope.apislot = slot;
    */

    coreconfig.GetSnapshotUri({ProfileToken: 'DEFAULT1'}).then(
      function (response) {
        $scope.snapshotUri = response.MediaUri.Uri;
      }
    );

    coreconfig.GetStreamUri({
      StreamSetup: {
        Stream: 'RTP-Unicast',
        Transport: {
          Protocol: 'UDP'
        },
        ProfileToken: coreconfig.getProfileTokenName(0)
      }
    }).then(function (response) {
      $scope.streamUri = response.MediaUri.Uri;
    });

    $scope.play = function () {
      $scope.$broadcast('playerEvent', {command: 'play'});
    };
    $scope.stop = function () {
      $scope.$broadcast('playerEvent', {command: 'stop'});
    };

    $scope.setSnapshotUri = function () {
      $scope.$broadcast('playerEvent', {command: 'setMediaUri', mediaUri: $scope.snapshotUri});
    };

    $scope.setQuicktimeUri = function () {
      $scope.$broadcast('playerEvent', {command: 'setMediaUri', mediaUri: $scope.streamUri});
    };
  });