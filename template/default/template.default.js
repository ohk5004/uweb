angular.module('template.default',
  ['ui.bootstrap', 'ui.utils', 'ui.router', 'ngAnimate']);

angular.module('template.default').config(function ($stateProvider) {
  $stateProvider.state('liveview', {
    url: '/liveview',
    templateUrl: 'template/default/partial/liveview/liveview.html',
    resolve: {
      'DefaultData': [ 'storage', function (storage) {
        return storage.promise.then(function (response) {
          return response.data;
        });
      }]
    }
  });
});

