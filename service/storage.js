angular.module('uWeb')
  .factory('storage', function ($http) {
    var data = [];
    var promise = $http.get('model/default.json').success(
      function (response) {
        data = response;
      }
    );

    var storage = {
      promise: promise,
      set: function (obj) {
        data = this.merge(this.get(), obj);
      },
      get: function (key) {
        if (!key) {
          return data;
        }
        var type = typeof key;
        var obj = data;
        if (type === 'string' || type === 'number') {
          key = String(key).replace(/\[(.*?)\]/g, function (m, key) {
            return '.' + key;
          }).split('.');

          var i, l;
          for (i = 0, l = key.length; i < l; i++) {
            if (obj.hasOwnProperty(key[i])) {
              obj = obj[key[i]];
            } else {
              // Todo:
              // Read Default Value
              return undefined;
            }
          }
          return obj;
        }
      },
      merge: function (target, src) {
        var array = Array.isArray(src);
        var dst = array && [] || {};
        var $this = this;
        var type;

        if (array) {
          target = target || [];
          dst = dst.concat(target);
          src.forEach(function (e, i) {
            type = typeof (dst[i]);
            if (type === 'undefined') {
              dst[i] = e;
            } else if (typeof e === 'object') {
              dst[i] = $this.merge(target[i], e);
            } else {
              if (target.indexOf(e) === -1) {
                dst.push(e);
              }
            }
          });
        } else {
          if (target && typeof target === 'object') {
            Object.keys(target).forEach(function (key) {
              dst[key] = target[key];
            });
          }
          Object.keys(src).forEach(function (key) {
            if (typeof src[key] !== 'object' || !src[key]) {
              dst[key] = src[key];
            } else {
              if (!target[key]) {
                dst[key] = src[key];
              } else {
                dst[key] = $this.merge(target[key], src[key]);
              }
            }
          });
        }
        return dst;
      }
    };

    return storage;
  });