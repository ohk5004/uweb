angular.module('uWeb')
  .factory('coreconfig', function ($q, $log, $injector, storage) {
    var api;
    var slots = { defaultSlot: 'onvif' };
    var coreconfig = {
      _get: function (key) {
        return storage.get(key);
      },
      _begin: function () {
        api = $injector.get(
          slots.currentSlot || slots.apiSlot || slots.defaultSlot
        );
      },
      _exit: function () {
        delete slots.currentSlot;
      },
      _findToken: function (obj, tokenName) {
        var i;
        for (i in obj) {
          if (obj[i]['@token'] === tokenName) {
            return obj[i];
          }
        }
        return undefined;
      },
      setSlot: function (slotname) {
        slots.apiSlot = slotname;
        return this;
      },
      Slot: function (slotname) {
        slots.currentSlot = slotname;
        return this;
      },
      getData: function (group) {
        this._begin();
        this._exit();
        return this._get(group);
      },
      getProfileTokenName: function (channel) {
        var tokenName = this._get('media.Profiles[' + channel + '][@token]');
        if (!tokenName) {
          $log.error('Wrong Channel');
          return false;
        }
        return tokenName;
      },
      GetSnapshotUri: function (request) {
        var printUsage = function () {
          $log.error('Parameter Error: Parameter must be like');
          $log.error(
            '{ProfileToken: \'DEFAULT1\'}'
          );
        };

        if (typeof request !== 'object' || !request.ProfileToken) {
          printUsage();
          return false;
        }

        this._begin();
        var deferred = $q.defer();
        var data = this._get();

        if (typeof api.GetSnapshotUri === 'function') {
          var current = this._findToken(
            data.media.Profile,
            request.ProfileToken
          );
          api.GetSnapshotUri(request).then(function (response) {
            current.MediaUri.Snapshot = response;
            storage.set(data);
            deferred.resolve({ MediaUri: response } || undefined);
          });
        }
        this._exit();
        return deferred.promise;
      },
      GetStreamUri: function (request) {
        var printUsage = function () {
          $log.error('Parameter Error: Parameter must be like');
          $log.error(
            '{ StreamSetup: { ' +
            'Stream: \'RTP-Unicast\'' +
            ', Transport: { Protocol: \'UDP\' } }' +
            ', ProfileToken: \'DEFAULT2\' }'
          );
        };

        if (typeof request !== 'object' ||
            !request.StreamSetup ||
            !request.StreamSetup.Stream ||
            !request.StreamSetup.Transport.Protocol ||
            !request.StreamSetup.ProfileToken) {
          printUsage();
          return false;
        }

        this._begin();
        var data = this._get();
        var deferred = $q.defer();

        if (typeof api.GetStreamUri === 'function') {
          var current = this._findToken(
            this._get('media.Profile'),
            request.StreamSetup.ProfileToken
          );
          api.GetStreamUri(request).then(function (response) {
            current.MediaUri.Stream[request.StreamSetup.Stream][request.StreamSetup.Transport.Protocol] = response;
            storage.set(data);
            deferred.resolve({ MediaUri: response } || undefined);
          });
        }
        this._exit();
        return deferred.promise;
      }
    };

    return coreconfig;
  });