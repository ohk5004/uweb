angular.module('uWeb')
  .directive('screen', function ($compile, uaparser) {
    return {
      restrict: 'E',
      scope: {
        mediaUri: '@',
        autostart: '@',
        class: '@'
      },
      compile: function (element, attrs) {
        var getPlayer = function () {
          var player;
          if (uaparser.isMobile()) {
            if (uaparser.getUA().device.model === 'iPhone') {
              player = 'snapshot';
            } else {
              player = 'embedding';
            }
          } else {
            if (uaparser.getUA().browser.name === 'IE') {
              player = 'activex';
            } else {
              player = 'quicktime';
            }

            // when quicktime is not installed...
          }
          return player || 'snapshot';
        };

        if (!attrs.player && !attrs.done) {
          element.attr('player', getPlayer());
          element.attr('done', 'true');
          return function (scope, el) {
            $compile(el)(scope);
          };
        }
        if (attrs.player) {
          element.attr(attrs.player, '');
          element.removeAttr('player');
          element.attr('done', 'true');
          return function (scope, el) {
            $compile(el)(scope);
          };
        }
      }
    };
  })
  .directive('snapshot', function () {
    return {
      restrict: 'A',
      templateUrl: 'partial/snapshot/snapshot.html'
    };
  })
  .directive('quicktime', function () {
    return {
      restrict: 'A',
      templateUrl: 'partial/quicktime/quicktime.html'
    };
  });
