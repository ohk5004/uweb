var template = 'template.default';

angular.module('uWeb', ['ui.bootstrap', 'ui.utils', 'ui.router', 'ngAnimate', template, 'api.onvif', 'api.uapi']);

angular.module('uWeb').config(function ($stateProvider, $urlRouterProvider) {
  /* Add New States Above */
//  $urlRouterProvider.otherwise('/liveview');
});

angular.module('uWeb').run(function ($rootScope) {
  $rootScope.safeApply = function (fn) {
    var phase = $rootScope.$$phase;
    if (phase === '$apply' || phase === '$digest') {
      if (fn && (typeof fn === 'function')) {
        fn();
      }
    } else {
      this.$apply(fn);
    }
  };
});
