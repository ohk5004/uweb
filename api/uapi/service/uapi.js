angular.module('api.uapi').factory('uapi', function ($q, $log, $http) {
  $log.log('Uapi Service');
  var uapi = {
    GetSnapshotUri: function () {
      var deferred = $q.defer();
      $http.get('model/uapi.json').success(function (response) {
        // sample
        var result = response.MediaUri.Snapshot;
        deferred.resolve(result);
      });
      return deferred.promise;
    },
    GetStreamUri: function (request) {
      var deferred = $q.defer();
      $http.get('model/uapi.json').success(function (response) {
        // sample
        var result = response.MediaUri.Stream[request.StreamSetup.Stream][request.StreamSetup.Transport.Protocol];
        deferred.resolve(result);
      });
      return deferred.promise;
    }
  };
  return uapi;
});