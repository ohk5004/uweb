angular.module('api.onvif').factory('onvif', function ($q, $log, $http) {
  $log.log('onvif Service');
  var onvif = {
    GetSnapshotUri: function () {
      var deferred = $q.defer();
      $http.get('model/onvif.json').success(function (response) {
        // sample
        var result = response.MediaUri.Snapshot;
        deferred.resolve(result);
      });
      return deferred.promise;
    },
    GetStreamUri: function (request) {
      var deferred = $q.defer();
      $http.get('model/onvif.json').success(function (response) {
        // sample
        var result = response.MediaUri.Stream[request.StreamSetup.Stream][request.StreamSetup.Transport.Protocol];
        deferred.resolve(result);
      });
      return deferred.promise;
    }
  };
  return onvif;
});