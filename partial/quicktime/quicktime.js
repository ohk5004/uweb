angular.module('uWeb')
  .controller('QuicktimeCtrl', function ($scope, $element, coreconfig) {
    var makeQuicktimePlayer = function (mediaUri, autoStart, width, height) {
      var objStr = ' <object CODEBASE="http://www.apple.com/qtactivex/qtplugin.cab"';
      if (width) {
        objStr += ' width=' + width;
      }
      if (height) {
        objStr += ' height=' + height;
      }
      objStr += '>';
      objStr += '<param name="src" value="images/qtposter.mov">';
      objStr += '<param name="qtsrc" value="' + mediaUri + '">';
      if (autoStart === 'true' || autoStart === 'false') {
        objStr += '<param name="autoplay" value="' + autoStart + '">';
      } else {
        objStr += '<param name="autoplay" value="false">';
      }
      objStr += '<param name="controller" value="false">';
      objStr += '<param name="type" value="video/quicktime">';
      objStr += '<param name="scale" value="tofit">';
      objStr += '<param name="target" value="myself">';
      objStr += '<param name="target" value="myself">';
      objStr += '<embed id="VideoScreen"';
      objStr += ' type="video/quicktime"';
      objStr += ' src="/images/qtposter.mov"';
      objStr += ' qtsrc="' + mediaUri + '"';
      if (autoStart === 'true' || autoStart === 'false') {
        objStr += ' autoplay="' + autoStart + '"';
      } else {
        objStr += ' autoplay="false"';
      }
      objStr += ' controller="false"';
      objStr += ' type="video/quicktime"';
//      objStr += ' scale="tofit"';
      objStr += ' target="myself"';
      objStr += ' plugin="quicktimeplugin"';
      objStr += ' cache="false"';
      objStr += ' pluginspage="http://www.apple.com/quicktime/download/"';
      objStr += ' loop="false"';
      if (width) {
        objStr += ' width=' + width;
      }
      if (height) {
        objStr += ' height=' + height;
      }
      objStr += '/>';

      return objStr;
    };

    // Todo:
    // Get Video Size
    coreconfig.GetStreamUri({
      StreamSetup: {
        Stream: 'RTP-Unicast',
        Transport: {
          Protocol: 'UDP'
        },
        ProfileToken: coreconfig.getProfileTokenName(0)
      }
    }).then(function (data) {
      var mediaUri = $scope.mediaUri || data.MediaUri.Uri;
      $element.html(makeQuicktimePlayer(mediaUri, $scope.autostart));
    });
    $scope.$on('playerEvent', function (event, args) {
      var player = document.getElementById('VideoScreen');
      switch (args.command) {
      case 'play':
        player.Play();
        break;
      case 'stop':
        player.Stop();
        break;
      case "setMediaUri":
        player.Stop();
        $element.html(makeQuicktimePlayer(args.mediaUri, 'true'));
        break;
      }
    });
  });