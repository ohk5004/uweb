angular.module('uWeb')
  .controller('SnapshotCtrl', function ($scope, $parse, $timeout, coreconfig) {
    'use strict';

    var SnapshotPlayer = function (snapshotUrl, container) {
      this.snapshotUrl = snapshotUrl;
      this.container = container;
      this.isPlaying = false;
    };

    SnapshotPlayer.prototype = {
      constructor: SnapshotPlayer,
      setImageContainer: function (container) {
        this.container = container;
      },
      setMediaUri: function (mediaUri) {
        this.snapshotUrl = mediaUri;
      },
      start: function () {
        if (this.isPlaying === true) {
          return;
        }
        var player = this;
        $(new Image())
          .load(function () {
            var buff = this;
            $parse(player.container).assign($scope, buff.src);
            $timeout(function () {
              if (player.isPlaying === false) {
                return;
              }
              buff.src = player.snapshotUrl + '?_=' + (new Date()).getTime();
            }, 100);
          })
          .error(function () {
            console.info('Image loading error...');
            player.isPlaying = false;
            return;
          })
          .prop('src', player.snapshotUrl);
        player.isPlaying = true;
      },
      stop: function () {
        this.isPlaying = false;
      }
    };

    var player;

    coreconfig.GetSnapshotUri({
      ProfileToken: coreconfig.getProfileTokenName(0)
    }).then(function (data) {
      var mediaUri = $scope.mediaUri || data.MediaUri.Uri;
      player = new SnapshotPlayer(mediaUri, 'mediaUri');
      if ($scope.autostart === 'true') {
        player.start();
      }
    });

    $scope.$on('playerEvent', function (event, args) {
      switch (args.command) {
      case 'play':
        player.start();
        break;
      case 'stop':
        player.stop();
        break;
      case "setMediaUri":
        player.stop();
        player.setMediaUri(args.mediaUri);
        player.start();
        break;
      }
    });
  });